/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clases;

import java.util.Objects;

/**
 *
 * @author Alumno
 */
public class Teclado {
    
    

    /**
     * @return the n
     */
    public static int getN() {
        return n;
    }

    /**
     * @param aN the n to set
     */
    public static void setN(int aN) {
        n = aN;
    }
    static int n =0;
    private int id;
    public String marca;
    public String numerodeserie;
    public String modelo;
    public String color;
    public int numerodeparte;
    public Teclado(String a,String b, String c,String d,int e){
       this.marca = a;
       this.numerodeserie= b;
       this.modelo= c;
       this.color= d;
       this.numerodeparte = e;
       this.id = n;
       n++;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the numerodeserie
     */
    public String getNumerodeserie() {
        return numerodeserie;
    }

    /**
     * @param numerodeserie the numerodeserie to set
     */
    public void setNumerodeserie(String numerodeserie) {
        this.numerodeserie = numerodeserie;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the numerodeparte
     */
    public int getNumerodeparte() {
        return numerodeparte;
    }

    /**
     * @param numerodeparte the numerodeparte to set
     */
    public void setNumerodeparte(int numerodeparte) {
        this.numerodeparte = numerodeparte;
    }
    public String toString(){
       return this.id+","+this.marca+","+this.numerodeserie+","+
               this.modelo+","+this.color+","+this.numerodeparte;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        hash = 97 * hash + Objects.hashCode(this.marca);
        hash = 97 * hash + Objects.hashCode(this.numerodeserie);
        hash = 97 * hash + Objects.hashCode(this.modelo);
        hash = 97 * hash + Objects.hashCode(this.color);
        hash = 97 * hash + this.numerodeparte;
        return hash;
    }
    
    /**
     *
     * @param other
     * @return
     */
    @Override
    public boolean equals(Object other){
         Teclado teclado = (Teclado)other;
         return teclado.getMarca().equals(this.marca) &&
                 teclado.getNumerodeserie().equals(this.numerodeserie) &&
         teclado.getModelo().equals(this.modelo);
                 
    }
    
    
}


